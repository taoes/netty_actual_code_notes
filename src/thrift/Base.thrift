namespace java com.zhoutao123.demo.thrift

// 定义别名
typedef i32 int
typedef i64 long


// 定义结构体
 struct News {
    1:required int id;
    2:required string title;
    3:optional string content;
 }

// 定义异常
 exception DataException{
    1:optional string message;
    2:optional string code;
 }

//定义服务
 service NewsOperatorService{
    News get(1:required int id) throws (1:DataException dataException);
    void remove(1:required int id) throws (1:DataException dataException);
 }
