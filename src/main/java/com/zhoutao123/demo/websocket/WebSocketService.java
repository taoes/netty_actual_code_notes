package com.zhoutao123.demo.websocket;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.stream.ChunkedWriteHandler;

/**
 * WebSocket 案例代码 运行服务器代码，打开src/webapp/websocket.html文件，观察WebSocket的效果
 *
 * @author tao
 */
public class WebSocketService {

  public static void main(String[] args) throws Exception {
    EventLoopGroup boosGroup = new NioEventLoopGroup();
    EventLoopGroup workGroup = new NioEventLoopGroup();
    try {

      ServerBootstrap serverBootstrap = new ServerBootstrap();

      serverBootstrap
          .group(boosGroup, workGroup)
          .channel(NioServerSocketChannel.class)
          .handler(new LoggingHandler(LogLevel.INFO))
          .childHandler(
              new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel socketChannel) throws Exception {
                  ChannelPipeline ch = socketChannel.pipeline();
                  ch.addLast("httpServerCodec", new HttpServerCodec());
                  // 分块写入处理
                  ch.addLast("chunkedWrite", new ChunkedWriteHandler());
                  // Http对象聚合
                  ch.addLast("httpObjectAggregator", new HttpObjectAggregator(8192));
                  // WebSocket服务协议处理器
                  ch.addLast(new WebSocketServerProtocolHandler("/ws"));
                  ch.addLast(new WebSocketServerHandle());
                }
              });

      ChannelFuture channelFuture = serverBootstrap.bind(8899).sync();
      channelFuture.channel().closeFuture().sync();
    } finally {
      boosGroup.shutdownGracefully();
      workGroup.shutdownGracefully();
    }
  }
}
