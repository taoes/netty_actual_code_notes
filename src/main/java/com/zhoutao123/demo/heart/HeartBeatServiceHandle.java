package com.zhoutao123.demo.heart;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleStateEvent;

/**
 * @author tao
 *     <p>心跳服务Handle
 */
public class HeartBeatServiceHandle extends ChannelInboundHandlerAdapter {

  /**
   * 用户事件被触发
   *
   * @param ctx
   * @param evt
   * @throws Exception
   */
  @Override
  public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
    if (evt instanceof IdleStateEvent) {
      IdleStateEvent event = (IdleStateEvent) evt;
      String eventType = null;
      switch (event.state()) {
        case READER_IDLE:
          eventType = "读空闲";
          break;
        case WRITER_IDLE:
          eventType = "写空闲";
          break;
        case ALL_IDLE:
          eventType = "读写空闲";
          break;
      }
      Channel channel = ctx.channel();
      System.out.println(channel.remoteAddress() + " 超时状态：" + eventType);
      ctx.close();
    } else {
      System.out.println("未知的事件类型");
    }
  }
}
