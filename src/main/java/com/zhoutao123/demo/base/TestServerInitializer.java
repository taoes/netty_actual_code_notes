package com.zhoutao123.demo.base;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpServerCodec;

public class TestServerInitializer extends ChannelInitializer<SocketChannel> {
  @Override
  protected void initChannel(SocketChannel ch) {
    ChannelPipeline pipeline = ch.pipeline();
    pipeline.addLast("HttpServerCodec", new HttpServerCodec());
    pipeline.addLast("TestServerHttpHandle", new TestServerHttpHandle());
  }
}
