package com.zhoutao123.demo.base;

import com.sun.jndi.toolkit.url.Uri;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;
import io.netty.util.CharsetUtil;

import java.net.URI;
import java.nio.charset.Charset;

public class TestServerHttpHandle extends SimpleChannelInboundHandler<HttpObject> {

  /**
   * 读取客户端请求，并处理返回相应
   *
   * @param ctx
   * @param msg
   * @throws Exception
   */
  @Override
  protected void channelRead0(ChannelHandlerContext ctx, HttpObject msg) throws Exception {
    if (msg instanceof HttpRequest) {
      HttpRequest request = (HttpRequest) msg;
      System.out.println("请求方式:" + request.method().name());
      URI uri = new URI(request.uri());
      if ("/favicon.ico".equals(uri.getPath())) {
        System.out.println("请求/favicon.ico文件");
        ctx.flush();
        return;
      }
      ByteBuf content = Unpooled.copiedBuffer("Hello,World.", CharsetUtil.UTF_8);
      FullHttpResponse fullHttpResponse =
          new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, content);
      fullHttpResponse.headers().set(HttpHeaderNames.CONTENT_TYPE, "text/plain");
      fullHttpResponse.headers().set(HttpHeaderNames.CONTENT_LENGTH, content.readableBytes());
      ctx.writeAndFlush(fullHttpResponse);
    }
  }
}
