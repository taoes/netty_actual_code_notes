package com.zhoutao123.demo.protobuf;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.concurrent.EventExecutorGroup;

public class ProtobufServiceHandle extends SimpleChannelInboundHandler<DataInfo.Student> {
  @Override
  protected void channelRead0(ChannelHandlerContext ctx, DataInfo.Student msg) throws Exception {
    System.out.println("服务端接收到消息:");
    System.out.print("\t" + msg.getAge());
    System.out.print("\t" + msg.getAddress());
    System.out.print("\t" + msg.getName());
  }
}
