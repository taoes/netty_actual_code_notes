package com.zhoutao123.demo.protobuf;

import com.google.protobuf.InvalidProtocolBufferException;

public class ProtoBufTest {

  public static void main(String[] args) throws InvalidProtocolBufferException {
    DataInfo.Student.Builder builder = DataInfo.Student.newBuilder();
    DataInfo.Student student = builder.setName("周涛涛").setAge(24).setAddress("江苏南京").build();
    byte[] student2BytesArray = student.toByteArray();
    DataInfo.Student parseObject = DataInfo.Student.parseFrom(student2BytesArray);
    // 输出测试结果
    System.out.println(parseObject.getName());
    System.out.println(parseObject.getAddress());
    System.out.println(parseObject.getAge());
    // 断言测试
    assert student.getName().equals(parseObject.getName());
    assert student.getAddress().equals(parseObject.getAddress());
    assert student.getAge() == student.getAge();
  }
}
