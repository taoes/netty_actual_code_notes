package com.zhoutao123.demo.protobuf;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class ProtobufClientHandle extends SimpleChannelInboundHandler<DataInfo.Student> {
  @Override
  protected void channelRead0(ChannelHandlerContext ctx, DataInfo.Student msg) throws Exception {}

  /**
   * channel 处于活动状态后向服务端发送消息
   *
   * @param ctx
   * @throws Exception
   */
  @Override
  public void channelActive(ChannelHandlerContext ctx) throws Exception {
    DataInfo.Student student =
        DataInfo.Student.newBuilder()
            .setName("周涛涛")
            .setAge(25)
            .setAddress("安徽省芜湖市九华北路171号")
            .build();
    ctx.channel().writeAndFlush(student);
  }
}
