package com.zhoutao123.demo.protobuf;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32FrameDecoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32LengthFieldPrepender;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ProtoBufClient {

  public static void main(String[] args) throws InterruptedException, IOException {
    EventLoopGroup group = new NioEventLoopGroup();

    try {
      Bootstrap bootstrap = new Bootstrap();
      bootstrap
          .group(group)
          .channel(NioSocketChannel.class)
          .handler(
              new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel socketChannel) throws Exception {
                  ChannelPipeline ch = socketChannel.pipeline();
                  ch.addLast(new ProtobufVarint32FrameDecoder());
                  ch.addLast(new ProtobufDecoder(DataInfo.Student.getDefaultInstance()));
                  ch.addLast(new ProtobufVarint32LengthFieldPrepender());
                  ch.addLast(new ProtobufEncoder());
                  ch.addLast(new ProtobufClientHandle());
                }
              });
      ChannelFuture future = bootstrap.connect("localhost", 8899).sync();
      future.channel().closeFuture().sync();

    } finally {
      group.shutdownGracefully();
    }
  }
}
