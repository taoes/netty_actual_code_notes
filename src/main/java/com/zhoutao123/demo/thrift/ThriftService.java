package com.zhoutao123.demo.thrift;

import org.apache.thrift.TProcessorFactory;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.server.THsHaServer;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.apache.thrift.transport.TTransportException;

public class ThriftService {

  public static void main(String[] argList) throws TTransportException {
    TNonblockingServerSocket socket = new TNonblockingServerSocket(8899);

    THsHaServer.Args args = new THsHaServer.Args(socket).minWorkerThreads(2).maxWorkerThreads(4);
    NewsOperatorService.Processor<NewOparatorSerrviceImpl> processor =
        new NewsOperatorService.Processor<>(new NewOparatorSerrviceImpl());

    args.protocolFactory(new TCompactProtocol.Factory());
    args.transportFactory(new TFastFramedTransport.Factory());
    args.processorFactory(new TProcessorFactory(processor));

    THsHaServer server = new THsHaServer(args);

    System.out.println("Service started!");
    server.serve();
  }
}
