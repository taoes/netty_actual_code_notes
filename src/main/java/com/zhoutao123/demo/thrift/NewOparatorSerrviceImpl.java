package com.zhoutao123.demo.thrift;

import org.apache.thrift.TException;

public class NewOparatorSerrviceImpl implements NewsOperatorService.Iface {
  @Override
  public News get(int id) throws DataException, TException {
    System.out.println("Get Function Got Client Param:" + id);
    News news = new News();
    news.setId(id);
    news.setContent("内容内容内容内容内容内容内容内容内容内容内容内容");
    news.setTitle("新闻标题");
    return news;
  }

  @Override
  public void remove(int id) throws DataException, TException {
    System.out.println("Remove Function Got Client Param ：" + id);
  }
}
