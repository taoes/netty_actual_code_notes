package com.zhoutao123.demo.thrift;

import org.apache.thrift.TException;
import org.apache.thrift.TProcessorFactory;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.server.THsHaServer;
import org.apache.thrift.transport.*;

public class ThriftClient {

  public static void main(String[] argList) throws TException {

    TTransport tTransport = new TFastFramedTransport(new TSocket("localhost", 8899), 600);
    TCompactProtocol tCompactProtocol = new TCompactProtocol(tTransport);
    NewsOperatorService.Client client = new NewsOperatorService.Client(tCompactProtocol);

    try {
      tTransport.open();
      News news = client.get(324175468);
      System.out.println(news.getId() + "   " + news.getTitle() + "  " + news.getContent());
      System.out.println("-------------------");
      client.remove(12);
    } finally {
      tTransport.close();
    }
  }
}
