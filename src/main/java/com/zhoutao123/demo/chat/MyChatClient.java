package com.zhoutao123.demo.chat;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.sctp.nio.NioSctpChannel;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MyChatClient {

  public static void main(String[] args) throws InterruptedException, IOException {
    EventLoopGroup group = new NioEventLoopGroup();

    try {
      Bootstrap bootstrap = new Bootstrap();
      bootstrap
          .group(group)
          .channel(NioSocketChannel.class)
          .handler(
              new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel socketChannel) throws Exception {
                  ChannelPipeline ch = socketChannel.pipeline();

                  ch.addLast(
                      "basedFrameClient",
                      new DelimiterBasedFrameDecoder(4096, Delimiters.lineDelimiter()));
                  ch.addLast("strDecoderClient", new StringDecoder());
                  ch.addLast("strEncoderClient", new StringEncoder());
                  ch.addLast(new MyChatClientHandle());
                }
              });
      ChannelFuture future = bootstrap.connect("localhost", 8899).sync();
      Channel channel = future.channel();
      BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
      while (true) {
        channel.writeAndFlush(bufferedReader.readLine() + "\r\n");
      }

    } finally {
      group.shutdownGracefully();
    }
  }
}
