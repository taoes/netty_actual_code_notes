package com.zhoutao123.demo.chat;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.EventExecutorGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

public class MyChatServerHandle extends SimpleChannelInboundHandler<String> {
  private static ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

  @Override
  protected void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {
    Channel channel = ctx.channel();
    channels.forEach(
        ch -> {
          if (ch != channel) {
            ch.writeAndFlush(channel.remoteAddress() + " 发送的消息：" + msg + "\r\n");
          } else {
            ch.writeAndFlush("[自己] " + msg + "\r\n");
          }
        });
  }

  @Override
  public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
    Channel channel = ctx.channel();
    channels.writeAndFlush("[服务器]:用户" + channel.remoteAddress() + "加入\r\n");
    channels.add(channel);
    int onlineCount = channels.size();
    channel.writeAndFlush("你好,欢迎加入聊天室.......(当前在线人数：" + onlineCount + ")\r\n");
  }

  @Override
  public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
    Channel channel = ctx.channel();
    channels.writeAndFlush("[服务器]:用户" + channel.remoteAddress() + "离开\r\n");
  }

  @Override
  public void channelActive(ChannelHandlerContext ctx) throws Exception {
    Channel channel = ctx.channel();
    System.out.println(channel.remoteAddress() + "上线");
  }

  @Override
  public void channelInactive(ChannelHandlerContext ctx) throws Exception {
    Channel channel = ctx.channel();
    System.out.println(channel.remoteAddress() + "下线");
  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
    cause.printStackTrace();
    ctx.close();
  }
}
