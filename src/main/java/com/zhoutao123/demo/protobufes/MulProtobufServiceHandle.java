package com.zhoutao123.demo.protobufes;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/** 多协议PrototBuf 传输协议 */
public class MulProtobufServiceHandle extends SimpleChannelInboundHandler<DataInfos.Message> {
  @Override
  protected void channelRead0(ChannelHandlerContext ctx, DataInfos.Message msg) throws Exception {
    System.out.println("服务端接收到消息:消息类型为 ---->" + msg.getType().name());
    if (DataInfos.Message.Type.STUDENT == msg.getType()) {
      System.out.println("姓名：" + msg.getStudent().getName());
      System.out.println("年龄：" + msg.getStudent().getAge());
      System.out.println("地址：" + msg.getStudent().getAddress());
    } else if (DataInfos.Message.Type.PERSON == msg.getType()) {
      System.out.println("姓名：" + msg.getPerson().getName());
      System.out.println("年龄：" + msg.getPerson().getAge());
    }
  }
}
