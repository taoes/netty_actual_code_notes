package com.zhoutao123.demo.protobufes;

import com.zhoutao123.demo.protobuf.DataInfo;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.Random;

public class MulProtobufClientHandle extends SimpleChannelInboundHandler<DataInfo.Student> {
  @Override
  protected void channelRead0(ChannelHandlerContext ctx, DataInfo.Student msg) throws Exception {}

  /**
   * channel 处于活动状态后向服务端发送消息
   *
   * @param ctx
   * @throws Exception
   */
  @Override
  public void channelActive(ChannelHandlerContext ctx) throws Exception {
    int randomInt = new Random().nextInt(2);

    DataInfos.Message message = null;
    if (randomInt == 0) {
      DataInfos.Student student =
          DataInfos.Student.newBuilder().setName("张三").setAge(12).setAddress("江苏省南京市建邺区").build();
      message =
          DataInfos.Message.newBuilder()
              .setType(DataInfos.Message.Type.STUDENT)
              .setStudent(student)
              .build();
    } else {
      DataInfos.Person person = DataInfos.Person.newBuilder().setName("李四").setAge(32).build();
      message =
          DataInfos.Message.newBuilder()
              .setType(DataInfos.Message.Type.PERSON)
              .setPerson(person)
              .build();
    }

    ctx.channel().writeAndFlush(message);
  }
}
